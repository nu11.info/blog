+++
author = "Nate Crisler"
category = "docs"
date = 2019-11-13T05:00:00Z
layout = "Post"
summary = "Splunk - Tomcat"
tags = ["tomcat errors", "splunk query", "splunk"]
title = "Tomcat response Errors"

+++
\# Tomcat Response errors

HTTP/HTTPS requests have over 200 errors within a 10 second period of time (we may want to consider tuning this, but let's see what 200 alerts on now.  
index=<tomcat_index>="tomcat:access:log"

    index=<tomcat_index> sourcetype="tomcat:access:log"
    | bucket _time span=10s
    | rex field=_raw "(?ms)^\[^\\"\\\\n\]*\\"(?P<firstLineofRequest>\[^\\"\]+)\\"\\\\s+(?P<httpStatusCode>\\\\d+)\\\\s+(?P<bytesSentCount>\[^ \]+)"
    | search httpStatusCode>399
    | stats count(httpStatusCode) AS STCount
    | search STCount>200

HTTP/HTTPS requests do not respond after 10 seconds

    index=<tomcat_index> sourcetype="tomcat:access:log"
    | bucket _time span=10s
    | rex field=_raw "(?ms)^\[^\\"\\\\n\]*\\"(?P<firstLineofRequest>\[^\\"\]+)\\"\\\\s+(?P<httpStatusCode>\\\\d+)\\\\s+(?P<bytesSentCount>\[^ \]+)"
    | search httpStatusCode>0
    | stats count(httpStatusCode) AS STCount
    | search STCount<0