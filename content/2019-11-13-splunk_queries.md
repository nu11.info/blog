---
layout: post
title: Splunk Queries
date: 2019-11-13 07:24
category:
- docs
author: 
tags:
- splunk
- tomcat
- stats
- bucket
- search-time-field-extraction
summary: 
menu:
  main:
    name: Splunk
    weight: 2

---
# Tomcat Response errors

#### HTTP/HTTPS requests have over 200 errors within a 10 second period of time (we may want to consider tuning this, but let's see what 200 alerts on now.

    sourcetype="tomcat:access:log"
    | bucket _time span=10s
    | rex field=_raw "(?ms)^[^\"\\n]*\"(?P<firstLineofRequest>[^\"]+)\"\\s+(?P<httpStatusCode>\\d+)\\s+(?P<bytesSentCount>[^ ]+)"
    | search httpStatusCode>399
    | stats count(httpStatusCode) AS STCount
    | search STCount>200

#### HTTP/HTTPS requests do not respond after 10 seconds

    sourcetype="tomcat:access:log"
    | bucket _time span=10s
    | rex field=_raw "(?ms)^[^\"\\n]*\"(?P<firstLineofRequest>[^\"]+)\"\\s+(?P<httpStatusCode>\\d+)\\s+(?P<bytesSentCount>[^ ]+)"
    | search httpStatusCode>0
    | stats count(httpStatusCode) AS STCount
    | search STCount<0